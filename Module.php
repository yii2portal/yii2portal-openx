<?php

namespace yii2portal\openx;

use Yii;

class Module extends \yii2portal\core\Module
{
    public $openxHost;
    public $openxBaseUrl;
    public $openxSiteId;
}